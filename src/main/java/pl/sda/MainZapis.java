package pl.sda;

import java.io.*;
import java.util.Scanner;

public class MainZapis {

    public static void main(String[] args) {

        try {
            PrintWriter writer = new PrintWriter("plik.txt");
            writer.println("Hello!");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


//        try(PrintWriter writer = new PrintWriter("plik.txt")){
//            writer.println("Dupa!");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }


        try (PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", true))) {
            writer.println("Dupa!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

