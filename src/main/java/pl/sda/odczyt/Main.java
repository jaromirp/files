package pl.sda.odczyt;

import java.io.File;
import java.io.*;

public class Main {
    //package pl.sda.odczyt_zad1;
    //public class Main {
    public static void main(String[] args) {

        File plik = new File("output_1.txt");
        if (!plik.exists()) {
            System.out.println("Plik nie istnieje...");
            return;
        } else {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(plik));
                String linia = reader.readLine();
                if (!linia.equals("Hello World!")) {
                    System.out.println("Plik posiada inną zawartość");
                    return;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            }
        }
        System.out.println("Plik istnieje i jest taki sam");
    }
}
