package pl.sda.odczyt;

import java.io.*;

public class Zad3Odczyt {
    //package pl.sda.odczyt_zad3;
    //public class zad3 {
    public static void main(String[] args) {

        int countSpace = 0;
        int nextLine = 0;
        String linia;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("output_3.txt"));

            while ((linia = reader.readLine()) != null)
                if (linia != null) {
                    nextLine++;
                    for (int i = 0; i < linia.length(); i++) {
                        if (linia.charAt(i) == ' ') {
                            countSpace++;
                        }
                    }
                }

            // To rozwiązanie jest dobre, tylko do czasu aż nie ma pustego akapitu
            System.out.println("Liczba wierszy: " + nextLine);
            System.out.printf("Liczba wyrazów: %d", nextLine + countSpace);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

