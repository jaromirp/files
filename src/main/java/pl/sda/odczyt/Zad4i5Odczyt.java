package pl.sda.odczyt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zad4i5Odczyt {
    //package pl.sda.odczyt_zad4;
    // public class Main {
    public static void main(String[] args) {

        int countSpace = 0;
        int nextLine = 0;
        String linia;

        try {
            BufferedReader reader = new BufferedReader(new FileReader("output_form.txt"));

            while ((linia = reader.readLine()) != null)
                if (linia != null) {
                    if (linia.startsWith("Odpowiedz: ")) {
                        System.out.println(linia);
                    }
                    if (linia.startsWith("Podaj swoje zarobki: ")) {
                        countSpace++;
                    }
                }

            System.out.println("Przeprowadzono ankiet: " + countSpace);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
