package pl.sda.odczyt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zad5bOdczyt {
    // public class Zad5_odczyt {
    public static void main(String[] args) {

        int licznikOdpowiedzi = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
            String liniaZWejscia;

            // czytaj plik dopóki mamy jakąś linię w pliku
            while ((liniaZWejscia = reader.readLine()) != null) {
                licznikOdpowiedzi++;
            }

            // 3 ponieważ mam 3 pytania.
            System.out.println("Ilosc formularzy = " + (licznikOdpowiedzi / 3));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

