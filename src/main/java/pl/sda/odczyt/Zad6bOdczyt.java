package pl.sda.odczyt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zad6bOdczyt {
    //  public class Zad6_odczyt {
    public static void main(String[] args) {

        int licznik_mezczyzn = 0;
        int wiek_mezczyzn = 0;
        int wzrost_mezczyzn = 0;


        int licznik_kobiet = 0;
        int wiek_kobiet = 0;
        int wzrost_kobiet = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
            String liniaZWejscia;

            // czytaj plik dopóki mamy jakąś linię w pliku
            while ((liniaZWejscia = reader.readLine()) != null) {
                if (liniaZWejscia.equals("m")) {
                    licznik_mezczyzn++;

                    liniaZWejscia = reader.readLine();
                    wiek_mezczyzn += Integer.parseInt(liniaZWejscia);

                    liniaZWejscia = reader.readLine();
                    wzrost_mezczyzn += Integer.parseInt(liniaZWejscia);

                } else if (liniaZWejscia.equals("k")) {
                    licznik_kobiet++;

                    liniaZWejscia = reader.readLine();
                    wiek_kobiet += Integer.parseInt(liniaZWejscia);

                    liniaZWejscia = reader.readLine();
                    wzrost_kobiet += Integer.parseInt(liniaZWejscia);
                }// nie rozpatruje
            }

            System.out.println("Sredni wiek mezczyzn = " + (wiek_mezczyzn / licznik_mezczyzn));
            System.out.println("Sredni wiek kobiet = " + (wiek_kobiet / licznik_kobiet));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

