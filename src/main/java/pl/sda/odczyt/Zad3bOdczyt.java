package pl.sda.odczyt;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Zad3bOdczyt {
    // public class Zad3_odczyt {
    public static void main(String[] args) {

        int licznik_slow = 0;
        int licznik_linii = 0;

        try (BufferedReader reader = new BufferedReader(new FileReader("output_5.txt"))) {
            String liniaZWejscia;

            // czytaj plik dopóki mamy jakąś linię w pliku
            while ((liniaZWejscia = reader.readLine()) != null) {
                liniaZWejscia = liniaZWejscia.trim();

                licznik_linii++;
                if (!liniaZWejscia.isEmpty()) {
                    String[] slowa = liniaZWejscia.split(" ");

                    // sumujemy dlugosc tablicy slow
//                    licznik_slow += slowa.length;
                    for (int i = 0; i < slowa.length; i++) {
                        if (!slowa[i].isEmpty()) {
                            licznik_slow++;
                        }
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


