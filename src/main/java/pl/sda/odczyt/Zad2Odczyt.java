package pl.sda.odczyt;

import java.io.*;

public class Zad2Odczyt {
    public static void main(String[] args) {

//        File plik = new File("output_2");

        try {
            BufferedReader reader = new BufferedReader(new FileReader("output_2.txt"));
            String linia = reader.readLine();

            System.out.println(linia);
            System.out.println(linia.toUpperCase());
            System.out.println(linia.toLowerCase());
            System.out.println(linia.trim());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

