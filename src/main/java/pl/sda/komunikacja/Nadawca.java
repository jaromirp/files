package pl.sda.komunikacja;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Nadawca {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String tekst;
        do {
            tekst = scanner.nextLine();

            try (PrintWriter writer = new PrintWriter(new FileWriter("plik.txt"))) {
                writer.println(tekst);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!tekst.equals("quit"));
    }
}
