package pl.sda.komunikacja;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Odbiorca {

    public static void main(String[] args) {

        boolean isWorking = true;
        String staraLinia = "";
        while (isWorking) {
            try (BufferedReader reader = new BufferedReader(new FileReader("plik.txt"))) {

                String linia = reader.readLine();
                if (!staraLinia.equals(linia)) {
                    System.out.println(linia);
                    staraLinia = linia;
                }

                Thread.sleep(100);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

