package pl.sda;

import java.io.*;
import java.util.Scanner;

public class MainOdczyt {
    public static void main(String[] args) {
        File plik = new File("plik.txt");
        if (plik.exists()) {
            System.out.println("Exists");
        } else {
            try {
                plik.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // wielkość pliku
        System.out.println(plik.length());

        // czas modyfikacji
        System.out.println(plik.lastModified());

        // czy jest katalogiem
        System.out.println(plik.isDirectory());

        // sciezka absolutna
        System.out.println(plik.getAbsolutePath());

        // sciezka relatywna
        System.out.println(plik.getPath());


        // opcja 1/ forma 1
        try {
            Scanner scanner = new Scanner(plik);

            if (scanner.hasNextLine()) {
                // jeśli nie zweryfikuje czy jest następna linia to metoda
                // nextLine() zwróci exception
                String linia = scanner.nextLine();

                System.out.println("Wypis:" + linia);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // opcja 2/ forma 2
        try {
            BufferedReader reader = new BufferedReader(new FileReader(plik));

            String linia = reader.readLine();
            // jeśli plik się skończył lub nie ma żadnej linii, to metoda
            // readLine() zwraca null
            if (linia != null) {
                System.out.println("Wypis:" + linia);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}


