package pl.sda;

//3. Należy napisać aplikację która w pętli while czyta ze Scannera wejście użytkownika z konsoli,
// a następnie linia po linii wypisuje tekst do pliku 'output_3.txt'.
// Aplikacja ma się zamykać po wpisaniu przez użytkownika komendy "quit".
//
//Pętla powinna być w try a nie try w pętli. - w przeciwnym razie w pętli

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zad3Zapis {
    public static void main(String[] args) {



            try {
                PrintWriter writer = new PrintWriter("output_3.txt");


                Scanner scanner = new Scanner(System.in);

                System.out.println("Wpisz tekst i naciśnij Enter");
                //
                String tekstWpisywany = "";
                while (!tekstWpisywany.equals("quit")) {
                    tekstWpisywany = scanner.nextLine();

                    writer.println(tekstWpisywany);

                }
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

