package pl.sda;

//Napisz aplikację w której:
// - wczytaj od użytkownika jedną linię tekstu
// - po wczytaniu linii otwórz plik o nazwie 'output_2.txt'
// - zapisz do pliku linię pobraną od użytkownika
// - zamknij plik
//
//stara treść:
//Napisz aplikację która otwiera plik 'output_2.txt',
// następnie czyta jedną linię tekstu od użytkownika i wpisuje tą linię do pliku. P
// o tej czynności plik ma się zamknąć, a następnie program ma się zakończyć.

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zad2Zapis {
    public static void main(String[] args) {

        try {
            PrintWriter writer = new PrintWriter("output_2.txt");
            Scanner scanner = new Scanner(System.in);

            System.out.println("Wpisz tekst i naciśnij Enter");
            //
            String tekstUzytkownika = scanner.nextLine();

            //String tekstUzytkownika;
            //tekstUzytkownika = nextline.
            writer.println(tekstUzytkownika);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

}
