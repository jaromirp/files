package pl.sda;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zad4bZapis {
    //public class Zad4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Czy chcesz wypelnic formularz?");
        String odpowiedz = scanner.nextLine();

        while (odpowiedz.toLowerCase().equals("tak")) {
            System.out.println("Płeć? (k/m)");
            String plec = scanner.nextLine();

            System.out.println("Wiek?");
            String wiek = scanner.nextLine();

            System.out.println("Wzrost?");
            String wzrost = scanner.nextLine();

            try (PrintWriter writer = new PrintWriter(new FileWriter("output_5.txt", true))) {
                writer.println(plec);
                writer.println(wiek);
                writer.println(wzrost);
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Czy chcesz wypelnic formularz?");
            odpowiedz = scanner.nextLine();
        }

    }
}

