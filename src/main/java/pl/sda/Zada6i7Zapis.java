package pl.sda;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Zada6i7Zapis {
    public static void main(String[] args) {
        try (PrintWriter writer = new PrintWriter(new FileWriter("output_form.txt", true))) {
            Scanner scanner = new Scanner(System.in);
            String fajrancik;
            do {
                String pytanie1 = "Jaki jest Twój wiek?";
                System.out.println(pytanie1);
                writer.println(pytanie1);
                String odp1 = scanner.nextLine();
                writer.println(odp1);
                writer.flush();
                String pytanie2 = "Jaki jest Twój wzrost?";
                System.out.println(pytanie2);
                writer.println(pytanie2);
                String odp2 = scanner.nextLine();
                writer.println(odp2);
                writer.flush();
                String pytanie3 = "Jaka jest Twoja płeć?";
                System.out.println(pytanie3);
                writer.println(pytanie3);
                String odp3 = scanner.nextLine();
                writer.println(odp3);
                writer.flush();
                if (odp3.toLowerCase().equals("mezczyzna")) {
                    String pytaniem = "Jakie marki samochodow lubisz najbardziej (max3)?";
                    System.out.println(pytaniem);
                    writer.println(pytaniem);
                    String odpm = scanner.nextLine();
                    writer.println(odpm);
                    writer.flush();
                } else if (odp3.toLowerCase().equals("kobieta")) {
                    String pytaniek = "Jakie marki obówia lubisz najbardziej (max3)?";
                    System.out.println(pytaniek);
                    writer.println(pytaniek);
                    String odpk = scanner.nextLine();
                    writer.println(odpk);
                    writer.flush();
                }
                System.out.println("Czy chcesz wypełnić kolejny formularz (tak/nie)?");
                fajrancik = scanner.nextLine();
            } while (!fajrancik.equals("nie"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
