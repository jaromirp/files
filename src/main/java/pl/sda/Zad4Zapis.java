package pl.sda;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

//4. Napisz program, a w tym programie wczytaj jedną linię ze skanera od użytkownika.
// Poproś użytkownika o 'adres pliku'. Po wpisaniu zweryfikuj czy wybrany plik/katalog istnieje,
// czy jest plikiem/katalogiem, jaki jest jego: rozmiar, czas ostatniej modyfikacji
// i czy mamy prawa do odczytu/zapisu do tego pliku/katalogu.

public class Zad4Zapis {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj podaj ścieżkę do pliku/folderu");
        String sciezka = scanner.nextLine();
        File plik = new File(sciezka);

        if (plik.exists()) {
            System.out.println("Exists");
        } else {
            try {
                plik.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Is a directory? Answer: " + plik.isDirectory());

        System.out.println("How big is this file/directory? Answer: " + plik.length());

        System.out.println("When it was modified last time? Answer: " + new SimpleDateFormat("dd-MM-yyyy HH-mm-ss").format(
                new Date(plik.lastModified())));

        System.out.println("Can We read the file? Answer :" + plik.canRead());

        System.out.println("Can we write the file? Answer :" + plik.canWrite());
    }
}



