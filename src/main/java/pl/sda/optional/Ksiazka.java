package pl.sda.optional;

public class Ksiazka {
    //package pl.sda.optional;

    //public class Ksiazka {
    private String tytul;
    private String authorsName;

    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public String getAuthorsName() {
        return authorsName;
    }

    public void setAuthorsName(String authorsName) {
        this.authorsName = authorsName;
    }
}

