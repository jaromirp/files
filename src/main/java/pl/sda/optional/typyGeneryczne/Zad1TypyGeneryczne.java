package pl.sda.optional.typyGeneryczne;

//1. Stwórz statyczną metodę generyczną, która 10 razy wypisuje na ekran metodę toString podanego parametru.
//
//* stwórz metodę generyczną ze zmienną ilością argumentów (varargs) która wykonuje pętle przez wszystkie obiekty z parametru
// i wypisuje je na ekran metodą toString().

public class Zad1TypyGeneryczne {
    public static void main(String[] args) {

        String T = "hfsdhjsdhfjsdfsdhfsdfsfsjdfsdfsfbs";
      metoda(T);
    }

    //public void wydrukuj() { zadanie 1 - podstawowe
    public static <T> void metoda(T obiekt) {
        for (int i = 0; i < 10; i++) {
            System.out.println(obiekt);

        }
    }

    //zadnie 1 rozwinięcie
    public static <T> void metodaVarArgs(T... obiekty) {
        //varargs to trablica
        for (T obiekt : obiekty) {
            //wypisuje każdy element 10 razy
            for (int i = 0; i < 10; i++) {
                System.out.println(obiekt);

            }
        }

    }

    public static <T> void metoda(T[] obiekty) {
        for (T obiekt : obiekty) {
            System.out.println(obiekt);
        }
    }
}
