package pl.sda.optional.typyGeneryczne;

//7. Napisz statyczną metodę generyczną, która jako argument przyjmuje (varargs) różne elementy,
// a następnie (wewnątrz metody) umieszcza elementy obu list w jednej liście i zwraca ją jako wynik.
// Metoda powinna być generyczna i przyjmować tylko listy tego samego typu (parametr varargs to : ArrayList<T>... listy)

public class Zad7TypyGeneryczne {

    public static void main(String[] args) {

        String T = "hfsdhjsdhfjsdfsdhfsdfsfsjdfsdfsfbs";
        String T2 = "fgsdfgsdfgsdfgsdfgsdfgfdgfdgdfgdf";
        metoda(T);
    }

    //public void wydrukuj() { zadanie 1 - podstawowe
    public static <T> void metoda(T obiekt) {
        for (int i = 0; i < 10; i++) {
            System.out.println(obiekt);

        }
    }

    //zadnie 1 rozwinięcie
    public static <T> void metodaVarArgs(T... obiekty) {
        //varargs to trablica
        for (T obiekt : obiekty) {
            //wypisuje każdy element 10 razy
            for (int i = 0; i < 10; i++) {
                System.out.println(obiekt);

            }
        }

    }

    public static <T> void metoda(T[] obiekty) {
        for (T obiekt : obiekty) {
            System.out.println(obiekt);
        }
    }
}

