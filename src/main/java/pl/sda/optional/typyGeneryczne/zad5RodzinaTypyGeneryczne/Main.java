package pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne;

import pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne.ICzlonekRodziny;

//public class Main {

/**
 * Utwórz interfejs ICzłonekRodziny i metodą przedstawSie():void.
 * Wykorzystaj interfejs i implementuj go w klasach Matka, Ojciec, Syn, Córka.
 * dla klasy Matka niech instancja wypisuje wartość “i am mother”,
 * dla klasy Ojciec niech instancja wypisuje wartość “i am your father”,
 * dla klasy Córka niech instancja wypisuje wartość “i am daughter ;) ”,
 * dla klasy Syn niech instancja wypisuje wartość “who’s asking?”
 * <p>
 * <p>
 * Dopisz w interfejsie dodatkową metodę jestDorosły():boolean która zwraca dla rodziców true,dla dzieci false.
 * Stwórz w mainie kilka instancji powyższych klas. Przechowaj je w tablicy, a następnie iteruj i wypisuj
 * w kolejnych liniach ich metody ‘przedstawSie()’ oraz “jestDorosły”
 * 1we wszystkich klasach dodaj pole imie.
 * Zmień metodę ‘przedstawSie()’ tak, aby poza treścią wypisywała również imie członka rodziny. Imie przypisuj w konstruktorze
 * Zamień metodę przedstawSie():void na defaultową. Domyślnie metoda ma wypisać “I am just a simple family member”.
 */
public class Main {
    public static void main(String[] args) {
        ICzlonekRodziny[] czlonekRodzinies = new ICzlonekRodziny[4];
        czlonekRodzinies[0] = new Tata();
        czlonekRodzinies[1] = new Corka();
        czlonekRodzinies[2] = new Mama();
        czlonekRodzinies[3] = new Syn();

        for (int i = 0; i < czlonekRodzinies.length; i++) {
            czlonekRodzinies[i].przedstawSie();

            if (czlonekRodzinies[i] instanceof Tata) {
                Tata ojciec = (Tata) czlonekRodzinies[i];
                ojciec.zawolajPoPiwo();
            }
        }

        Tata znaleziony = znajdzOjca(czlonekRodzinies);
        if (znaleziony != null) {
            znaleziony.przedstawSie();
        }
    }

    private static <T extends ICzlonekRodziny> Tata znajdzOjca(T[] rodzinka) {
        for (int i = 0; i < rodzinka.length; i++) {
            if (rodzinka[i] instanceof Tata) {
                return (Tata) rodzinka[i];
            }
        }

        return null;
    }
}
