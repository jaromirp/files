package pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne;

//public class Tata {
public class Tata implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem ojciec!");
    }

    public void zawolajPoPiwo() {
        System.out.println("Przynieś piwko dziecko!");
    }
}
