package pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne;

//public class Matka {
public class Mama implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem Mama!");
    }

    public void zawolajPoPiwo() {
        System.out.println("Przynieś piwko dziecko dla Taty!");
    }

}
