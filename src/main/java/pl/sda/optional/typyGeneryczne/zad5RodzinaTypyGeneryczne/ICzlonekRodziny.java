package pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne;

//public class ICzlonekRodziny {
public interface ICzlonekRodziny {
    public void przedstawSie();

    public default boolean czyJestDorosly() {
        return true;
    }

    public default boolean aKtoPyta() {
        return true;
    }
}



