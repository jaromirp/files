package pl.sda.optional.typyGeneryczne.zad5RodzinaTypyGeneryczne;

//public class Corka {
public class Corka implements ICzlonekRodziny {
    @Override
    public void przedstawSie() {
        System.out.println("Jestem córka!");
    }

    public void zawolajPoPiwo() {
        System.out.println("Przynieś piwko dziecko!");
    }
}
