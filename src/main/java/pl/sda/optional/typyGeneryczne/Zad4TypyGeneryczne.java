package pl.sda.optional.typyGeneryczne;

//4. Stwórz metodę generyczną kóra dla dowolnych dwóch parametrów (dziedziczących po klasie Number (wykorzystaj metodę generyczną ze słówkiem extends) )
// zwraca ich sumę (jako double). Metoda ma przyjmować dwie liczby typu T i zwracać wynik typu double.

import pl.sda.optional.para.Czlowiek;

public class Zad4TypyGeneryczne {
    //public class Main {
    public static void main(String[] args) {
        Czlowiek czlowiek = new Czlowiek("marian");
        sumaLiczb(123L, 5);
    }

    public static <T extends Number> Double sumaLiczb(T liczba1, T liczba2) {
        double wynik = liczba1.doubleValue() + liczba2.doubleValue();
        return wynik;
    }
}



