package pl.sda.optional.typyGeneryczne;

//3. Stwórz klasę generyczną Para, która przyjmuje dwa obiekty - obiekt prawy i lewy. Niech klasa będzie generyczna.
// Typy obiektów (lewego i prawego) klasy mają być podane przy tworzeniu obiektu (generyczne).
// Stwórz kilka obiektów tego typu a następnie dodaj je do tablicy.
//
//3B*. Stwórz metodę generyczną 'znajdźNiepuste(Para[]):Para[]' która iteruje tablicę obiektów typu 'Para'
// i znajduje tylko niepuste pary (obiekt prawy i lewy mają być niepuste (!=null).

public class Zad3TypygenerycznePara {
    public class Para<P, L extends Ptak> {
        private P prawy;
        private L lewy;

        public Para(P prawy, L lewy) {
            this.prawy = prawy;
            this.lewy = lewy;
        }

        public P getPrawy() {
            return prawy;
        }

        public void setPrawy(P prawy) {
            this.prawy = prawy;
        }

        public L getLewy() {
            return lewy;
        }

        public void setLewy(L lewy) {
            this.lewy = lewy;
        }

        @Override
        public String toString() {
            return "Para{" +
                    "prawy=" + prawy +
                    ", lewy=" + lewy +
                    '}';
        }

        public boolean czyNiepusta() {
            return lewy != null && prawy != null;
        }
    }
}


