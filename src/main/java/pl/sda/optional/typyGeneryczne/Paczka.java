package pl.sda.optional.typyGeneryczne;
//2. Stwórz klasę generyczną Pudełko. Pudełko przechowuje dowolny obiekt generyczny.
// Dodaj pole typu generycznego 'T'. Stwórz konstruktor który przyjmuje jako parametr obiekt generyczny (przypisz jego wartość do pola).
// Dodaj getter oraz setter do klasy.
//
//* dodaj do klasy metodę "czyPudełkoJestPuste():boolean" która zwraca informację true - jeśli pudełko jest puste
// (czyli obiekt generyczny jest ==null) oraz false gdy nie jest puste (obiekt [pole] generyczne jest !=null).

//public class Zad2TypyGeneryczne<T> {
public class Paczka<T> {  //
    private static final Object ObiektWPaczce = 0;
    private T obiektWPaczce;

    public Paczka(T obiektWPaczce) {
        this.obiektWPaczce = obiektWPaczce;
    }

    public T getObiektWPaczce() {
        return obiektWPaczce;
    }

    public void setObiektWPaczce(T obiektWPaczce) {
        this.obiektWPaczce = obiektWPaczce;

    }

    public boolean czyPudelkoJestPuset() {
        return ObiektWPaczce == null;

    }

}
