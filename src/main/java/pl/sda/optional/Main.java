package pl.sda.optional;

import java.util.Optional;
import java.util.OptionalInt;

public class Main {
    ///package pl.sda.optional;
//
    //public class Main {
    public static void main(String[] args) {
        Biblioteka biblioteka = new Biblioteka();

        Optional<Ksiazka> pudelko = biblioteka.getKsiazka("Qvo vadis");
        if (pudelko.isPresent()) {
            Ksiazka ksiazka = pudelko.get();
            System.out.println(ksiazka.getAuthorsName());
        }

        Optional<String> pudelkoDrugie = biblioteka.getAuthorsName("Qvo vadis");
        if (pudelkoDrugie.isPresent()) {
            String authorsName = pudelkoDrugie.get();
            System.out.println(authorsName);
        }
    }
}


