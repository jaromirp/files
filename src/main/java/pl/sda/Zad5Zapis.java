package pl.sda;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

//Stwórz klasę formularz. Formularz reprezentuje odpowiedzi których udzielił użytkownik.
// Aplikacja ma po uruchomieniu rozpocząć od zadawania pytań użytkownikowi.
// Po wpisaniu danych(odpowiedzi na pytania) zapisz te odpowiedzi do pliku (output_form.txt) w odpowiednim formacie
// (przez format mamy na myśli zawartość - np. oddziel pytania i odpowiedzi tak, aby byly pisane w nowych liniach).
// Poproś użytkownika o: wiek, wzrost, płeć (kobieta/mezczyzna), zarobki i zadaj dwa dodatkowe pytania.
// Po czynności zamknij plik i program.

public class Zad5Zapis {
    public static void main(String[] args) {

        try (PrintWriter writer = new PrintWriter(new FileWriter("output_form.txt", false))) {
            Scanner scanner = new Scanner(System.in);
            String pytanie1 = "Jaki jest Twój wiek?";
            System.out.println(pytanie1);
            writer.println(pytanie1);
            String odp1 = scanner.nextLine();
            writer.println(odp1);
            writer.flush();
            String pytanie2 = "Jaki jest Twój wzrost?";
            System.out.println(pytanie2);
            writer.println(pytanie2);
            String odp2 = scanner.nextLine();
            writer.println(odp2);
            writer.flush();
            String pytanie3 = "Jaka jest Twoja płeć?";
            System.out.println(pytanie3);
            writer.println(pytanie3);
            String odp3 = scanner.nextLine();
            writer.println(odp3);
            writer.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}

