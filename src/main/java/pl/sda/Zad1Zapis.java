package pl.sda;

//Napisz aplikację która otwiera plik 'output_1.txt', zapisuje do niego "Hello World!" a następnie zamyka plik,
// po czym aplikacja ma się zamknąć (ma się naturalnie skończyć).

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Zad1Zapis {
    public static void main(String[] args) {

        try {
            PrintWriter writer = new PrintWriter("output_1.txt");
            writer.println("Hello World!");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

}
